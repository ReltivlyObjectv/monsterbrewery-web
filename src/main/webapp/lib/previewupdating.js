function checkForDuplicateSkills() {
    //Check all selected skills, then highlight any duplicates in red
    console.log("Checking for duplicate skills");
    //TODO
}
function checkForDuplicateLanguages() {
    //Check all selected skills, then highlight any duplicates in red
    console.log("Checking for duplicate languages");
    //TODO
}
function applyPronounsToPreview() {
    //Replace pronound placeholders with actual text
    var pronoun = $('select[name="pronoun"] option:selected').val();
    var formName = $('input[name="monstername"]').val();
    var name = "".concat(
            pronoun.includes("PROPER") ? "" : "the ",
            pronoun.includes("PROPER") ? formName : formName.toString().toLowerCase()
    );
    var namecap = "".concat(
            pronoun.includes("PROPER") ? "" : "The ",
            pronoun.includes("PROPER") ? formName : formName.toString().toLowerCase()
    );
    var he = pronoun.includes("FEMALE") ? "she" : pronoun.includes("IT") ? "it" : pronoun.includes("SWARM") ? "it" : "he";
    var hecap = pronoun.includes("FEMALE") ? "She" : pronoun.includes("IT") ? "It" : pronoun.includes("SWARM") ? "It" : "He";
    var him = pronoun.includes("FEMALE") ? "her" : pronoun.includes("IT") ? "it" : pronoun.includes("SWARM") ? "it" : "him";
    var himcap = pronoun.includes("FEMALE") ? "Her" : pronoun.includes("IT") ? "It" : pronoun.includes("SWARM") ? "It" : "Him";
    var his = pronoun.includes("FEMALE") ? "hers" : pronoun.includes("IT") ? "its" : pronoun.includes("SWARM") ? "its" : "his";
    var hiscap = pronoun.includes("FEMALE") ? "Hers" : pronoun.includes("IT") ? "Its" : pronoun.includes("SWARM") ? "Its" : "His";
    
    var innerHTML = $("#statblock").html();
    innerHTML = innerHTML.replaceAll("{name}",name);
    innerHTML = innerHTML.replaceAll("{namecap}",namecap);
    innerHTML = innerHTML.replaceAll("{he}",he);
    innerHTML = innerHTML.replaceAll("{hecap}",hecap);
    innerHTML = innerHTML.replaceAll("{him}",him);
    innerHTML = innerHTML.replaceAll("{himcap}",himcap);
    innerHTML = innerHTML.replaceAll("{his}",his);
    innerHTML = innerHTML.replaceAll("{hiscap}",hiscap);
    $("#statblock").html(innerHTML);
}
function setUseSpellcaster() {
    if ($("#spellcaster .spellcastingenabled").is(':checked')) {
        $('#spellcaster input[type="text"]').prop("disabled", false);
        $('#spellcaster input[type="number"]').prop("disabled", false);
        $('#spellcaster select').prop("disabled", false);
    } else {
        $('#spellcaster input[type="text"]').prop("disabled", true);
        $('#spellcaster input[type="number"]').prop("disabled", true);
        $('#spellcaster select').prop("disabled", true);
    }
}
function updateSpellSuggestions() {
    //Update each spell name input to suggest spells of that level
    $("#spells .ui-state-default").each(function() {
        var spellName = $(this).find(".spellname");
        var spellLevel = $(this).find(".spelllevel").val();
        var newListValue = "level".concat(spellLevel,"spells");
        spellName.attr("list", newListValue);
    });
}
function updateProficiencyBonus() {
    //Update recommended proficiency bonus
    var challengeRating = $(".challengerating option:selected").html();
    var proficiencyBonus = "Unknown CR";
    switch(challengeRating) {
        case "0":
        case "1/8":
        case "1/4":
        case "1/2":
        case "1":
        case "2":
        case "3":
        case "4":
            proficiencyBonus = "+2";
            break;
        case "5":
        case "6":
        case "7":
        case "8":
            proficiencyBonus = "+3";
            break;
        case "9":
        case "10":
        case "11":
        case "12":
            proficiencyBonus = "+4";
            break;
        case "13":
        case "14":
        case "15":
        case "16":
            proficiencyBonus = "+5";
            break;
        case "17":
        case "18":
        case "19":
        case "20":
            proficiencyBonus = "+6";
            break;
        case "21":
        case "22":
        case "23":
        case "24":
            proficiencyBonus = "+7";
            break;
        case "25":
        case "26":
        case "27":
        case "28":
            proficiencyBonus = "+8";
            break;
        case "29":
        case "30":
            proficiencyBonus = "+9";
            break;
    }
    $("#proficiencybonus").html(proficiencyBonus);
}
function updateActionPods() {
    //Show and hide action components to reflect the action type
    $("#actions .ui-state-default").each(function() {
        var actionType = $(this).find(".actiontype option:selected").val();
        switch (actionType) {
            case "melee":
                $(this).find(".attacktype").show();
                $(this).find(".rangedtype").hide();
                $(this).find(".attackdamage").show();
                $(this).find(".meleedetails").show();
                $(this).find(".fireforgetdetails").hide();
                $(this).find(".areadetails").hide();
                $(this).find(".nonareadetails").show();
                break;
            case "ranged":
                $(this).find(".attacktype").show();
                $(this).find(".rangedtype").show();
                $(this).find(".attackdamage").show();
                $(this).find(".meleedetails").hide();
                var rangedType = $(this).find(".rangedtype option:selected").val();
                switch (rangedType) {
                    case "fireforget":
                        $(this).find(".fireforgetdetails").show();
                        $(this).find(".areadetails").hide();
                        $(this).find(".nonareadetails").show();
                        break;
                    case "area":
                        $(this).find(".fireforgetdetails").hide();
                        $(this).find(".areadetails").show();
                        $(this).find(".nonareadetails").hide();
                        break;
                    default:
                        break;
                }
                break;
            case "other":
            default:
                $(this).find(".attacktype").hide();
                $(this).find(".rangedtype").hide();
                $(this).find(".attackdamage").hide();
                $(this).find(".meleedetails").hide();
                $(this).find(".fireforgetdetails").hide();
                $(this).find(".areadetails").hide();
                $(this).find(".nonareadetails").hide();
                break;
        }
    });
}
function updatePreview() {
    console.log("Updating preview.");
    
    updateProficiencyBonus();
    updateActionPods();
    updateSpellSuggestions();
    checkForDuplicateSkills();
    checkForDuplicateLanguages();
    setUseSpellcaster();
    
    //Basic Info
    if (true) {
        $(".monstername").html($('input[name="monstername"]').val());
        $(".rendersize").html($('.monstersize option:selected').text().toLowerCase());
        $(".rendertype").html($('input[name="monstertype"]').val().toLowerCase());
        $(".rendertag").html("("+$('input[name="monstertag"]').val().toLowerCase()+")");
        $(".renderalignment").html($('.monsteralignment option:selected').val().toLowerCase());
        $(".renderarmorclass").html($('input[name="armorclass"]').val().toLowerCase());
        var hitPointString;
        try {
            var hitPointDiceCount = parseInt($('input[name="hitdicecount"]').val());
            var hitPointDiceType = parseInt($('.hitdicetype option:selected').val());
            var hitPointModifier = parseInt($('input[name="hitdicemodifier"]').val());
            var hitPointTotal = Math.ceil(((hitPointDiceType*.5)+.5)*hitPointDiceCount+hitPointModifier);

            hitPointString = "".concat(hitPointTotal," (",hitPointDiceCount,"d",hitPointDiceType,hitPointModifier === 0?"":" + "+hitPointModifier,")");
        } catch (exception) {
            console.log(exception);
            hitPointString = "NaN Exception";
        }
        $(".renderhitpoints").html(hitPointString);
        $(".renderspeed").html($('input[name="speed"]').val());
    }
    
    //Challenge Rating
    if (true) {
        var challengeRating = $('.challengerating option:selected').html();
        var challengeXP;
        switch (challengeRating) {
            case "0":
                challengeXP = 10;
                break;
            case "1/8":
                challengeXP = 25;
                break;
            case "1/4":
                challengeXP = 50;
                break;
            case "1/2":
                challengeXP = 100;
                break;
            case "1":
                challengeXP = 200;
                break;
            case "2":
                challengeXP = 450;
                break;
            case "3":
                challengeXP = 700;
                break;
            case "4":
                challengeXP = 1100;
                break;
            case "5":
                challengeXP = 1800;
                break;
            case "6":
                challengeXP = 2300;
                break;
            case "7":
                challengeXP = 2900;
                break;
            case "8":
                challengeXP = 3900;
                break;
            case "9":
                challengeXP = 5000;
                break;
            case "10":
                challengeXP = 5900;
                break;
            case "11":
                challengeXP = 7200;
                break;
            case "12":
                challengeXP = 8400;
                break;
            case "13":
                challengeXP = 10000;
                break;
            case "14":
                challengeXP = 11500;
                break;
            case "15":
                challengeXP = 13000;
                break;
            case "16":
                challengeXP = 15000;
                break;
            case "17":
                challengeXP = 18000;
                break;
            case "18":
                challengeXP = 20000;
                break;
            case "19":
                challengeXP = 22000;
                break;
            case "20":
                challengeXP = 25000;
                break;
            case "21":
                challengeXP = 33000;
                break;
            case "22":
                challengeXP = 41000;
                break;
            case "23":
                challengeXP = 50000;
                break;
            case "24":
                challengeXP = 62000;
                break;
            case "25":
                challengeXP = 75000;
                break;
            case "26":
                challengeXP = 90000;
                break;
            case "27":
                challengeXP = 105000;
                break;
            case "28":
                challengeXP = 120000;
                break;
            case "29":
                challengeXP = 135000;
                break;
            case "30":
                challengeXP = 155000;
                break;
            default:
                console.log("Cannot find xp for ",challengeXP);
                challengeXP = 0;
        }
        var challengeString = challengeRating.concat(" (", challengeXP," XP)");
        $(".renderchallenge").html(challengeString);
    }
    
    //Ability Scores
    if (true) {
        $(".renderstr").html($('input[name="strscore"]').val());
        var strScoreMod = Math.floor(parseInt($('input[name="strscore"]').val())/2)-5;
        $(".renderstrmod").html("(".concat(strScoreMod<0?strScoreMod:"+".concat(strScoreMod),")"));

        $(".renderdex").html($('input[name="dexscore"]').val());
        var dexScoreMod = Math.floor(parseInt($('input[name="dexscore"]').val())/2)-5;
        $(".renderdexmod").html("(".concat(dexScoreMod<0?dexScoreMod:"+".concat(dexScoreMod),")"));

        $(".rendercon").html($('input[name="conscore"]').val());
        var conScoreMod = Math.floor(parseInt($('input[name="conscore"]').val())/2)-5;
        $(".renderconmod").html("(".concat(conScoreMod<0?conScoreMod:"+".concat(conScoreMod),")"));

        $(".renderint").html($('input[name="intscore"]').val());
        var intScoreMod = Math.floor(parseInt($('input[name="intscore"]').val())/2)-5;
        $(".renderintmod").html("(".concat(intScoreMod<0?intScoreMod:"+".concat(intScoreMod),")"));

        $(".renderwis").html($('input[name="wisscore"]').val());
        var wisScoreMod = Math.floor(parseInt($('input[name="wisscore"]').val())/2)-5;
        $(".renderwismod").html("(".concat(wisScoreMod<0?wisScoreMod:"+".concat(wisScoreMod),")"));

        $(".rendercha").html($('input[name="chascore"]').val());
        var chaScoreMod = Math.floor(parseInt($('input[name="chascore"]').val())/2)-5;
        $(".renderchamod").html("(".concat(chaScoreMod<0?chaScoreMod:"+".concat(chaScoreMod),")"));
    }
    
    //Saving Throws
    if (true) {
        var savingThrowsString = "";
        try {
            var strSave = parseInt($('input[name="strsave"]').val());
            var dexSave = parseInt($('input[name="dexsave"]').val());
            var conSave = parseInt($('input[name="consave"]').val());
            var intSave = parseInt($('input[name="intsave"]').val());
            var wisSave = parseInt($('input[name="wissave"]').val());
            var chaSave = parseInt($('input[name="chasave"]').val());
            if (strSave !== 0) {
                if (savingThrowsString.length !== 0) {
                    savingThrowsString += ", ";
                }
                strSave += Math.floor(parseInt($('input[name="strscore"]').val())/2)-5;
                savingThrowsString += "Str " + (strSave < 0 ? strSave : "+".concat(strSave));
            }
            if (dexSave !== 0) {
                if (savingThrowsString.length !== 0) {
                    savingThrowsString += ", ";
                }
                dexSave += Math.floor(parseInt($('input[name="dexscore"]').val())/2)-5;
                savingThrowsString += "Dex " + (dexSave < 0 ? dexSave : "+".concat(dexSave));
            }
            if (conSave !== 0) {
                if (savingThrowsString.length !== 0) {
                    savingThrowsString += ", ";
                }
                conSave += Math.floor(parseInt($('input[name="conscore"]').val())/2)-5;
                savingThrowsString += "Con " + (conSave < 0 ? conSave : "+".concat(conSave));
            }
            if (intSave !== 0) {
                if (savingThrowsString.length !== 0) {
                    savingThrowsString += ", ";
                }
                intSave += Math.floor(parseInt($('input[name="intscore"]').val())/2)-5;
                savingThrowsString += "Int " + (intSave < 0 ? intSave : "+".concat(intSave));
            }
            if (wisSave !== 0) {
                if (savingThrowsString.length !== 0) {
                    savingThrowsString += ", ";
                }
                savingThrowsString += "Wis " + (wisSave < 0 ? wisSave : "+".concat(wisSave));
            }
            if (chaSave !== 0) {
                if (savingThrowsString.length !== 0) {
                    savingThrowsString += ", ";
                }
                savingThrowsString += "Cha " + (chaSave < 0 ? chaSave : "+".concat(chaSave));
            }
        } catch (exception) {
            savingThrowsString = "NaN Exception";
        }
        $(".rendersavingthrows").html(savingThrowsString);
        if (savingThrowsString.length === 0) {
            //Hide
            $(".rendersavingthrowsline").hide();
        } else {
            //Show
            $(".rendersavingthrowsline").show();
        }
    }
    
    //Senses
    if (true) {
        var passivePerception = $('input[name="passiveperception"]').val();
        var blindsight = 0;
        var darkvision = 0;
        var truesight = 0;
        var tremorsense = 0;
        var sensesString = "passive perception ".concat(passivePerception);

        try {
            blindsight = parseInt($('input[name="blindsight"]').val());
        } catch (exception) {
            blindsight = "NaN Exception";
        }
        if (blindsight !== 0) {
            sensesString = sensesString.concat(", Blindsight ",blindsight," ft.");
        }

        try {
            darkvision = parseInt($('input[name="darkvision"]').val());
        } catch (exception) {
            darkvision = "NaN Exception";
        }
        if (darkvision !== 0) {
            sensesString = sensesString.concat(", Darkvision ",darkvision," ft.");
        }

        try {
            truesight = parseInt($('input[name="truesight"]').val());
        } catch (exception) {
            truesight = "NaN Exception";
        }
        if (truesight !== 0) {
            sensesString = sensesString.concat(", Truesight ",truesight," ft.");
        }

        try {
            tremorsense = parseInt($('input[name="tremorsense"]').val());
        } catch (exception) {
            tremorsense = "NaN Exception";
        }
        if (tremorsense !== 0) {
            sensesString = sensesString.concat(", Tremorsense ",tremorsense," ft.");
        }
        $(".rendersenses").html(sensesString);
    }
    
    //Skills
    if (true) {
        try {
            var acrobatics = 0;
            var animalHandling = 0;
            var arcana = 0;
            var athletics = 0;
            var deception = 0;
            var history = 0;
            var insight = 0;
            var intimidation = 0;
            var investigation = 0;
            var medicine = 0;
            var nature = 0;
            var perception = 0;
            var performance = 0;
            var persuasion = 0;
            var religion = 0;
            var sleightOfHand = 0;
            var stealth = 0;
            var survival = 0;
            
            var strScoreMod = Math.floor(parseInt($('input[name="strscore"]').val())/2)-5;
            var dexScoreMod = Math.floor(parseInt($('input[name="dexscore"]').val())/2)-5;
            var conScoreMod = Math.floor(parseInt($('input[name="conscore"]').val())/2)-5;
            var intScoreMod = Math.floor(parseInt($('input[name="intscore"]').val())/2)-5;
            var wisScoreMod = Math.floor(parseInt($('input[name="wisscore"]').val())/2)-5;
            var chaScoreMod = Math.floor(parseInt($('input[name="chascore"]').val())/2)-5;
            
            $(".skillproficiencies").find("tr").each(function() {
                var newSkill = $(this).find("option:selected").val();
                var newSkillMod = $(this).find("input").val();
                switch (newSkill) {
                    case "acrobatics":
                        acrobatics = parseInt(newSkillMod);
                        break;
                    case "animalhandling":
                        animalHandling = parseInt(newSkillMod);
                        break;
                    case "arcana":
                        arcana = parseInt(newSkillMod);
                        break;
                    case "athletics":
                        athletics = parseInt(newSkillMod);
                        break;
                    case "deception":
                        deception = parseInt(newSkillMod);
                        break;
                    case "history":
                        history = parseInt(newSkillMod);
                        break;
                    case "insight":
                        insight = parseInt(newSkillMod);
                        break;
                    case "intimidation":
                        intimidation = parseInt(newSkillMod);
                        break;
                    case "investigation":
                        investigation = parseInt(newSkillMod);
                        break;
                    case "medicine":
                        medicine = parseInt(newSkillMod);
                        break;
                    case "nature":
                        nature = parseInt(newSkillMod);
                        break;
                    case "perception":
                        perception = parseInt(newSkillMod);
                        break;
                    case "performance":
                        performance = parseInt(newSkillMod);
                        break;
                    case "persuasion":
                        persuasion = parseInt(newSkillMod);
                        break;
                    case "religion":
                        religion = parseInt(newSkillMod);
                        break;
                    case "sleightofhand":
                        sleightOfHand = parseInt(newSkillMod);
                        break;
                    case "stealth":
                        stealth = parseInt(newSkillMod);
                        break;
                    case "survival":
                        survival = parseInt(newSkillMod);
                        break;
                    default:
                        console.log("Exception: Unrecognized Skill ".concat(newSkill));
                        throw "Exception: Unrecognized Skill ".concat(newSkill);
                        break;
                }
            });
            var skillsString = "";
            if (acrobatics !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                var 
                skillsString = skillsString.concat("Acrobatics ", acrobatics+dexScoreMod < 0 ? "" : "+", acrobatics+dexScoreMod);
            }
            if (animalHandling !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Animal Handling ", animalHandling+chaScoreMod < 0 ? "" : "+", animalHandling+chaScoreMod);
            }
            if (arcana !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Arcana ", arcana+intScoreMod < 0 ? "" : "+", arcana+intScoreMod);
            }
            if (athletics !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Athletics ", athletics+strScoreMod < 0 ? "" : "+", athletics+strScoreMod);
            }
            if (deception !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Deception ", deception+chaScoreMod < 0 ? "" : "+", deception+chaScoreMod);
            }
            if (history !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("History ", history+intScoreMod < 0 ? "" : "+", history+intScoreMod);
            }
            if (insight !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Insight ", insight+wisScoreMod < 0 ? "" : "+", insight+wisScoreMod);
            }
            if (intimidation !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Intimidation ", intimidation+chaScoreMod < 0 ? "" : "+", intimidation+chaScoreMod);
            }
            if (investigation !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Investigation ", investigation+intScoreMod < 0 ? "" : "+", investigation+intScoreMod);
            }
            if (medicine !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Medicine ", medicine+wisScoreMod < 0 ? "" : "+", medicine+wisScoreMod);
            }
            if (nature !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Nature ", nature+intScoreMod < 0 ? "" : "+", nature+intScoreMod);
            }
            if (perception !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Perception ", perception+wisScoreMod < 0 ? "" : "+", perception+wisScoreMod);
            }
            if (performance !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Performance ", performance+chaScoreMod < 0 ? "" : "+", performance+chaScoreMod);
            }
            if (persuasion !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Persuasion ", persuasion+chaScoreMod < 0 ? "" : "+", persuasion+chaScoreMod);
            }
            if (religion !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Religion ", religion+intScoreMod < 0 ? "" : "+", religion+intScoreMod);
            }
            if (sleightOfHand !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                var 
                skillsString = skillsString.concat("Sleight of Hand ", sleightOfHand+dexScoreMod < 0 ? "" : "+", sleightOfHand+dexScoreMod);
            }
            if (stealth !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                var 
                skillsString = skillsString.concat("Stealth ", stealth+dexScoreMod < 0 ? "" : "+", stealth+dexScoreMod);
            }
            if (survival !== 0) {
                if (skillsString !== "") {
                    skillsString = skillsString.concat(", ");
                }
                skillsString = skillsString.concat("Survival ", survival+wisScoreMod < 0 ? "" : "+", survival+wisScoreMod);
            }
            if (skillsString.length === 0) {
                //Hide
                $(".renderskillsline").hide();
            } else {
                //Show
                $(".renderskillsline").show();
                $(".renderskills").html(skillsString);
            }
        } catch (exception) {
            $(".renderskills").html("NaN Exception");
        }
    }

    //Conditions Modifiers
    if (true) {
        var conResist = "";
        var conImmune = "";
        var conVulner = "";
        
        var blinded = $('select[name="blinded"] option:selected').html();
        if (blinded !== "Unchanged") {
            if (blinded === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "blinded";
            } else if (blinded === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "blinded";
            } else if (blinded === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "blinded";
            }
        }
        
        var charmed = $('select[name="charmed"] option:selected').html();
        if (charmed !== "Unchanged") {
            if (charmed === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "charmed";
            } else if (charmed === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "charmed";
            } else if (charmed === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "charmed";
            }
        }
        
        var deafened = $('select[name="deafened"] option:selected').html();
        if (deafened !== "Unchanged") {
            if (deafened === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "deafened";
            } else if (deafened === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "deafened";
            } else if (deafened === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "deafened";
            }
        }
        
        var encumbered = $('select[name="encumbered"] option:selected').html();
        if (encumbered !== "Unchanged") {
            if (encumbered === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "encumbered";
            } else if (encumbered === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "encumbered";
            } else if (encumbered === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "encumbered";
            }
        }
        
        var exhausted = $('select[name="exhausted"] option:selected').html();
        if (exhausted !== "Unchanged") {
            if (exhausted === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "exhausted";
            } else if (exhausted === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "exhausted";
            } else if (exhausted === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "exhausted";
            }
        }
        
        var frightened = $('select[name="frightened"] option:selected').html();
        if (frightened !== "Unchanged") {
            if (frightened === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "frightened";
            } else if (frightened === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "frightened";
            } else if (frightened === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "frightened";
            }
        }
        
        var grappled = $('select[name="grappled"] option:selected').html();
        if (grappled !== "Unchanged") {
            if (grappled === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "grappled";
            } else if (grappled === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "grappled";
            } else if (grappled === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "grappled";
            }
        }
        
        var incorporeal = $('select[name="incorporeal"] option:selected').html();
        if (incorporeal !== "Unchanged") {
            if (incorporeal === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "incorporeal";
            } else if (incorporeal === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "incorporeal";
            } else if (incorporeal === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "incorporeal";
            }
        }
        
        var intoxicated = $('select[name="intoxicated"] option:selected').html();
        if (intoxicated !== "Unchanged") {
            if (intoxicated === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "intoxicated";
            } else if (intoxicated === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "intoxicated";
            } else if (intoxicated === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "intoxicated";
            }
        }
        
        var invisible = $('select[name="invisible"] option:selected').html();
        if (invisible !== "Unchanged") {
            if (invisible === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "invisible";
            } else if (invisible === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "invisible";
            } else if (invisible === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "invisible";
            }
        }
        
        var paralyzed = $('select[name="paralyzed"] option:selected').html();
        if (paralyzed !== "Unchanged") {
            if (paralyzed === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "paralyzed";
            } else if (paralyzed === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "paralyzed";
            } else if (paralyzed === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "paralyzed";
            }
        }
        
        var petrified = $('select[name="petrified"] option:selected').html();
        if (petrified !== "Unchanged") {
            if (petrified === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "petrified";
            } else if (petrified === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "petrified";
            } else if (petrified === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "petrified";
            }
        }
        
        var poisoned = $('select[name="poisoned"] option:selected').html();
        if (poisoned !== "Unchanged") {
            if (poisoned === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "poisoned";
            } else if (poisoned === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "poisoned";
            } else if (poisoned === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "poisoned";
            }
        }
        
        var prone = $('select[name="prone"] option:selected').html();
        if (prone !== "Unchanged") {
            if (prone === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "prone";
            } else if (prone === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "prone";
            } else if (prone === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "prone";
            }
        }
        
        var restrained = $('select[name="restrained"] option:selected').html();
        if (restrained !== "Unchanged") {
            if (restrained === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "restrained";
            } else if (restrained === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "restrained";
            } else if (restrained === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "restrained";
            }
        }
        
        var stunned = $('select[name="stunned"] option:selected').html();
        if (stunned !== "Unchanged") {
            if (stunned === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "stunned";
            } else if (stunned === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "stunned";
            } else if (stunned === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "stunned";
            }
        }
        
        var unconscious = $('select[name="unconscious"] option:selected').html();
        if (unconscious !== "Unchanged") {
            if (unconscious === "Resistant") {
                if (conResist !== "") {
                    conResist+= ", ";
                }
                conResist += "unconscious";
            } else if (unconscious === "Immune") {
                if (conImmune !== "") {
                    conImmune+= ", ";
                }
                conImmune += "unconscious";
            } else if (unconscious === "Vulnerable") {
                if (conVulner !== "") {
                    conVulner+= ", ";
                }
                conVulner += "unconscious";
            }
        }
        
        if (conResist !== "") {
            $(".renderconresistline").show();
            $(".renderconresist").html(conResist);
        } else {
            $(".renderconresistline").hide();
        }
        if (conImmune !== "") {
            $(".renderconimmuneline").show();
            $(".renderconimmune").html(conImmune);
        } else {
            $(".renderconimmuneline").hide();
        }
        if (conVulner !== "") {
            $(".renderconvulnerline").show();
            $(".renderconvulner").html(conVulner);
        } else {
            $(".renderconvulnerline").hide();
        }
    }
    
    //Damage Modifiers
    if (true) {
        var damResist = "";
        var damImmune = "";
        var damVulner = "";
        
        var nonmagic = $('select[name="nonmagic"] option:selected').html();
        if (nonmagic !== "Unchanged") {
            if (nonmagic === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "bludgeoning, piercing, and slashing from nonmagical attacks";
            } else if (nonmagic === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "bludgeoning, piercing, and slashing from nonmagical attacks";
            } else if (nonmagic === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "bludgeoning, piercing, and slashing from nonmagical attacks";
            }
        }
        
        var acid = $('select[name="acid"] option:selected').html();
        if (acid !== "Unchanged") {
            if (acid === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "acid";
            } else if (acid === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "acid";
            } else if (acid === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "acid";
            }
        }
        
        var bludgeoning = $('select[name="bludgeoning"] option:selected').html();
        if (bludgeoning !== "Unchanged") {
            if (bludgeoning === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "bludgeoning";
            } else if (bludgeoning === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "bludgeoning";
            } else if (bludgeoning === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "bludgeoning";
            }
        }
        
        var cold = $('select[name="cold"] option:selected').html();
        if (cold !== "Unchanged") {
            if (cold === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "cold";
            } else if (cold === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "cold";
            } else if (cold === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "cold";
            }
        }
        
        var fire = $('select[name="fire"] option:selected').html();
        if (fire !== "Unchanged") {
            if (fire === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "fire";
            } else if (fire === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "fire";
            } else if (fire === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "fire";
            }
        }
        
        var force = $('select[name="force"] option:selected').html();
        if (force !== "Unchanged") {
            if (force === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "force";
            } else if (force === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "force";
            } else if (force === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "force";
            }
        }
        
        var lightning = $('select[name="lightning"] option:selected').html();
        if (lightning !== "Unchanged") {
            if (lightning === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "lightning";
            } else if (lightning === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "lightning";
            } else if (lightning === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "lightning";
            }
        }
        
        var necrotic = $('select[name="necrotic"] option:selected').html();
        if (necrotic !== "Unchanged") {
            if (necrotic === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "necrotic";
            } else if (necrotic === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "necrotic";
            } else if (necrotic === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "necrotic";
            }
        }
        
        var piercing = $('select[name="piercing"] option:selected').html();
        if (piercing !== "Unchanged") {
            if (piercing === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "piercing";
            } else if (piercing === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "piercing";
            } else if (piercing === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "piercing";
            }
        }
        
        var poison = $('select[name="poison"] option:selected').html();
        if (poison !== "Unchanged") {
            if (poison === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "poison";
            } else if (poison === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "poison";
            } else if (poison === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "poison";
            }
        }
        
        var psychic = $('select[name="psychic"] option:selected').html();
        if (psychic !== "Unchanged") {
            if (psychic === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "psychic";
            } else if (psychic === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "psychic";
            } else if (psychic === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "psychic";
            }
        }
        
        var radiant = $('select[name="radiant"] option:selected').html();
        if (radiant !== "Unchanged") {
            if (radiant === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "radiant";
            } else if (radiant === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "radiant";
            } else if (radiant === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "radiant";
            }
        }
        
        var slashing = $('select[name="slashing"] option:selected').html();
        if (slashing !== "Unchanged") {
            if (slashing === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "slashing";
            } else if (slashing === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "slashing";
            } else if (slashing === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "slashing";
            }
        }
        
        var thunder = $('select[name="thunder"] option:selected').html();
        if (thunder !== "Unchanged") {
            if (thunder === "Resistant") {
                if (damResist !== "") {
                    damResist+= ", ";
                }
                damResist += "thunder";
            } else if (thunder === "Immune") {
                if (damImmune !== "") {
                    damImmune+= ", ";
                }
                damImmune += "thunder";
            } else if (thunder === "Vulnerable") {
                if (damVulner !== "") {
                    damVulner+= ", ";
                }
                damVulner += "thunder";
            }
        }
        
        if (damResist !== "") {
            $(".renderdamresistline").show();
            $(".renderdamresist").html(damResist);
        } else {
            $(".renderdamresistline").hide();
        }
        if (damImmune !== "") {
            $(".renderdamimmuneline").show();
            $(".renderdamimmune").html(damImmune);
        } else {
            $(".renderdamimmuneline").hide();
        }
        if (damVulner !== "") {
            $(".renderdamvulnerline").show();
            $(".renderdamvulner").html(damVulner);
        } else {
            $(".renderdamvulnerline").hide();
        }
    }
    
    //Languages
    if (true) {
        var languageString = "";
        $("#languages input").each(function() {
            var newLanguage = $(this).val();
            languageString = languageString.concat(
                languageString === "" ? "" : ", ",
                newLanguage
            );
        });
        $(".renderlanguages").html(languageString);
    }
    
    //Abilities
    if (true) {
        var renderReactionsString = "";
        $("#abilities .ui-state-default").each(function() {
            var newAbilityName = $(this).children("input").val();
            var newAbilityDescription = $(this).children("textarea").val();
            renderReactionsString = renderReactionsString.concat(
                '<p><strong><em>',
                newAbilityName,
                '. </em></strong>',
                newAbilityDescription,
                '</p>'
            );
        });
        $(".renderabilities").html(renderReactionsString);
    }
    
    //Spellcaster
    if ($("#spellcaster .spellcastingenabled").is(':checked')) {
        var spellcastingLevel = parseInt($('input[name="spellcastinglevel"]').val());
        var spellcastingToHit = parseInt($('input[name="spellcastingtohit"]').val());
        var spellcastingHeader = "{namecap} is a ".concat(
                spellcastingLevel,
                spellcastingLevel === 1 ? "st" : spellcastingLevel === 2 ? "nd" : spellcastingLevel === 3 ? "rd" : "th",
                "-level spellcaster with the spellcasting ability being ",
                $('select[name="spellcastingability"] option:selected').html(),
                " (spell save DC ",
                $('input[name="spellcastingdc"]').val(),
                ", ",
                spellcastingToHit < 0 ? "" : "+",
                spellcastingToHit,
                " to hit with spell attacks). {namecap} has the following ",
                $('input[name="spellcastingclass"]').val().toLowerCase(),
                " spells prepared:<br><br>"
                );
        var spellList = ["","","","","","","","","",""];
        var hasCastBeforeCombat = false;
        $("#spells").find("li").each(function() {
            var spellName = $(this).find(".spellname").val();
            if (spellName.endsWith("*")) {
                hasCastBeforeCombat = true;
            }
            var spellLevel = parseInt($(this).find(".spelllevel").val());
            if (spellList[spellLevel] !== "") {
                spellList[spellLevel] += ", ";
            }
            spellList[spellLevel] += spellName.toString().toLowerCase();
        });
        var allSpellListsString = "";
        for (var i = 0; i <= 9; i++) {
            var spellSlots = parseInt($(".spelllevels").find('input[name=level'.concat(i,']')).val());
            var spellString = spellList[i];
            var spellRowString = "".concat(
                i === 0 ? "Cantrips" : i === 1 ? "1st" : i === 2 ? "2nd" : i === 3 ? "3rd" : "".concat(i,"th"),
                " (",
                i === 0 ? "at will" : "".concat(spellSlots,spellSlots === 1 ? " slot" : " slots"),
                "): ",
                spellString
                );
            if (spellString === "" && i === 0) {
                //No cantrips; do nothing
            } else if (spellSlots !== 0 || spellString !== "") {
                if (spellString === "") {
                    spellRowString += "(none known)";
                }
                allSpellListsString += spellRowString.concat("<br>");
            }
        }
        var spellcastingHTML = "<p><strong>Spellcasting. </strong>".concat(
                spellcastingHeader,
                allSpellListsString,
                hasCastBeforeCombat ? "<br>&ast;{namecap} casts these spells on {him}self before combat." : "",
                "</p>"
                );
        $(".renderabilities").append(spellcastingHTML);
        
    } else {
        console.log("Skipping Spellcaster");
    }
    
    //Actions
    if (true) {
        var renderActionsString = "";
        $("#actions .ui-state-default").each(function() {
            var appendString = "";
            try {
                var actionType = $(this).find(".actiontype option:selected").val();
                var actionName = $(this).find('input[name="actionname"]').val();
                var actionDescription = $(this).find('textarea').val();
                var attackDamageDiceCount = parseInt($(this).find(".dicecounter").val());
                var attackDamageDiceType = $(this).find(".dicetype option:selected").val();
                var attackDamageBonus = parseInt($(this).find(".dicemodifier").val());
                var attackDamageType = $(this).find(".damagetype").val();
                var attackType = $(this).find(".attacktype option:selected").val();
                var attackRange = $(this).find(".rangemelee").val();
                var toHit = parseInt($(this).find(".tohit").val());
                var averageDamage = Math.ceil(((attackDamageDiceType*.5)+.5)*attackDamageDiceCount+attackDamageBonus);
                var rangedType = $(this).find(".rangedtype option:selected").val();
                var rangeMin = $(this).find(".rangeshort").val();
                var rangeMax = $(this).find(".rangelong").val();
                var attackShape = $(this).find(".shapetype option:selected").html();
                var attackSize = $(this).find(".areasize").val();
                
                switch (actionType) {
                    case "melee":
                        appendString = "<p><strong><em>".concat(
                                actionName,
                                ".</em></strong> ",
                                "<em>Melee ",
                                attackType,
                                " Attack:</em> ",
                                toHit < 0 ? "" : "+",
                                toHit,
                                " to hit, reach ",
                                attackRange,
                                " ft., one target. <em>Hit: </em>",
                                averageDamage,
                                " (",
                                attackDamageDiceCount,
                                "d",
                                attackDamageDiceType,
                                attackDamageBonus === 0 ? "" : attackDamageBonus < 0 ? attackDamageBonus : "+".concat(attackDamageBonus),
                                ") ",
                                attackDamageType,
                                " damage. ",
                                actionDescription,
                                "</p>"
                        );
                        break;
                    case "ranged":
                        switch (rangedType) {
                            case "fireforget":
                                appendString = "<p><strong><em>".concat(
                                        actionName,
                                        ".</em></strong> <em> Ranged ",
                                        attackType,
                                        " Attack:</em> ",
                                        toHit < 0 ? "" : "+",
                                        toHit,
                                        " to hit, range ",
                                        rangeMin,
                                        "/",
                                        rangeMax,
                                        " ft., one target. <em>Hit: </em>",
                                        averageDamage,
                                        " (",
                                        attackDamageDiceCount,
                                        "d",
                                        attackDamageDiceType,
                                        attackDamageBonus === 0 ? "" : attackDamageBonus < 0 ? attackDamageBonus : "+".concat(attackDamageBonus),
                                        ") ",
                                        attackDamageType,
                                        " damage. ",
                                        actionDescription,
                                        "</p>"
                                );
                                break;
                            case "area":
                                appendString = "<p><strong><em>".concat(
                                        actionName,
                                        ".</em></strong> <em> Ranged ",
                                        attackType,
                                        " Attack:</em> ",
                                        attackSize,
                                        " ft. ",
                                        attackShape,
                                        ". <em>Hit: </em>",
                                        averageDamage,
                                        " (",
                                        attackDamageDiceCount,
                                        "d",
                                        attackDamageDiceType,
                                        attackDamageBonus === 0 ? "" : attackDamageBonus < 0 ? attackDamageBonus : "+".concat(attackDamageBonus),
                                        ") ",
                                        attackDamageType,
                                        " damage. ",
                                        actionDescription,
                                        "</p>"
                                );
                                
                                
                                break;
                            default:
                                break;
                        }
                        break;
                    case "other":
                    default:
                        appendString += "<p><strong><em>".concat(
                            actionName,
                            ".</em></strong> ",
                            actionDescription,
                            "</p>"
                        );
                        break;
                }
            } catch (exception) {
                appendString = "<p>NaN Error</p>";
            }
            renderActionsString += appendString;
            /*
            var newActionName = $(this).children("input").val();
            var newAbilityDescription = $(this).children("textarea").val();
            renderActionsString = renderActionsString.concat(
                '<p><strong><em>',
                newActionName,
                '. </em></strong>',
                newAbilityDescription,
                '</p>'
            );
             */
        });
        $(".renderactions").html(renderActionsString);
        if ("" === renderActionsString) {
            $(".renderactionstitle").hide();
        } else {
            $(".renderactionstitle").show();
        }
    }
    
    //Reactions
    if (true) {
        var renderReactionsString = "";
        $("#reactions .ui-state-default").each(function() {
            var newReactionName = $(this).children("input").val();
            var newReactionDescription = $(this).children("textarea").val();
            renderReactionsString = renderReactionsString.concat(
                '<p><strong><em>',
                newReactionName,
                '. </em></strong>',
                newReactionDescription,
                '</p>'
            );
        });
        $(".renderreactions").html(renderReactionsString);
        if ("" === renderReactionsString) {
            $(".renderreactionstitle").hide();
        } else {
            $(".renderreactionstitle").show();
        }
    }

    //Legendary Actions
    if (true) {
        var legendaryCount = 0;
        try {
            legendaryCount = parseInt($('input[name="legendarycount"]').val());
        } catch (exception) {
            legendaryCount = "NaN";
        }
        if (legendaryCount !== 0) {
            var renderLegendaryString = "".concat(
                "<p>{namecap} can take ",
                legendaryCount,
                " legendary action",
                legendaryCount === 1 ? "" : "s",
                " choosing from the options below. Only one legendary action ",
                "option can be used at a time and only at the end of another ",
                "creature’s turn. {namecap} regains spent legendary actions ",
                "at the start of {his} turn.</p>"
            );
            $("#legendaryactions .ui-state-default").each(function() {
                var newLegendaryName = $(this).children("input").val();
                var newLegemdaryDescription = $(this).children("textarea").val();
                renderLegendaryString = renderLegendaryString.concat(
                    '<p><strong><em>',
                    newLegendaryName,
                    '. </em></strong>',
                    newLegemdaryDescription,
                    '</p>'
                );
            });
            $(".renderlegendarytitle").show();
            $(".renderlegendary").show();
            $(".renderlegendary").html(renderLegendaryString);
        } else {
            $(".renderlegendarytitle").hide();
            $(".renderlegendary").hide();
        }
    }

    //Pronoun Application
    applyPronounsToPreview();
}