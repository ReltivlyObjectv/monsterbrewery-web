function addBlankSkill() {
    addSkill("acrobatics",0);
}
function addSkill(skill, modifier) {
    console.log("Adding Skills Pod.");
    var newSkillHTML = "".concat( //TODO make the specified skill selected and update the default value to the specified one
        '<tr><td><select onchange="updatePreview();">',
        '<option',skill==="acrobatics"?" selected":"",' value="acrobatics">Acrobatics (Dex)</option>',
        '<option',skill==="animalhandling"?" selected":"",' value="animalhandling">Animal Handling (Wis)</option>',
        '<option',skill==="arcana"?" selected":"",' value="arcana">Arcana (Int)</option>',
        '<option',skill==="athletics"?" selected":"",' value="athletics">Athletics (Str)</option>',
        '<option',skill==="deception"?" selected":"",' value="deception">Deception (Cha)</option>',
        '<option',skill==="history"?" selected":"",' value="history">History (Int)</option>',
        '<option',skill==="insight"?" selected":"",' value="insight">Insight (Wis)</option>',
        '<option',skill==="animalhandling"?" selected":"",' value="intimidation">Intimidation (Cha)</option>',
        '<option',skill==="intimidation"?" selected":"",' value="investigation">Investigation (Int)</option>',
        '<option',skill==="medicine"?" selected":"",' value="medicine">Medicine (Wis)</option>',
        '<option',skill==="nature"?" selected":"",' value="nature">Nature (Int)</option>',
        '<option',skill==="perception"?" selected":"",' value="perception">Perception (Wis)</option>',
        '<option',skill==="performance"?" selected":"",' value="performance">Performance (Cha)</option>',
        '<option',skill==="persuasion"?" selected":"",' value="persuasion">Persuasion (Cha)</option>',
        '<option',skill==="religion"?" selected":"",' value="religion">Religion (Int)</option>',
        '<option',skill==="sleightofhand"?" selected":"",' value="sleightofhand">Sleight of Hand (Dex)</option>',
        '<option',skill==="stealth"?" selected":"",' value="stealth">Stealth (Dex)</option>',
        '<option',skill==="survival"?" selected":"",' value="survival">Survival (Wis)</option>',
        '</select></td><td><input type="number" min="-50" max="50" value="',
        modifier,
        '"></td><td><button class="deletebutton">X</button></td></tr>'
    );
    $(".skillproficiencies").append(newSkillHTML);
    checkForDuplicateSkills();
    updatePreview();
}
function addBlankLanguage() {
    addLanguage("New Language");
}
function addLanguage(language) {
    console.log("Adding Language Pod.");
    var newLanguageHTML = "".concat(
        '<li class="ui-state-default"><input type="text" value="',
        language,
        '" class="listitemtitle"><button class="deletebutton">X</button></li>'
    );
    $("#languages").append(newLanguageHTML);
    updatePreview();
}
function addBlankAbility() {
    addAbility("New Ability", "Description goes here.");
}
function addAbility(name, description) {
    console.log("Adding Ability Pod.");
    var newAbilityHTML = "".concat(
        '<li class="ui-state-default"><input type="text" value="',
        name,
        '" class="listitemtitle"><button class="deletebutton">X</button><textarea>',
        description,
        '</textarea></li>'
    );
    $("#abilities").append(newAbilityHTML);
    updatePreview();
}
function addBlankAction() {
    addAction("New Action", "Description goes here.", "other", 1, 4, 0, "acid", "Weapon", 5, 0, "fireforget", 150, 600, "cone", 10);
}
function addAction(name, description, actionType, damageDiceCount, damageDiceType, 
        damageBonus, damageType, attackType, attackRange, toHit, rangedType, 
        rangeMin, rangeMax, attackShape, areaSize) {
    console.log("Adding Action Pod.");
    var newActionHTML = "".concat(
        '<li class="ui-state-default">',
        '<input type="text" name="actionname" value="',
        name,
        '" class="listitemtitle"><button class="deletebutton">X</button>',
        '<select class="actiontype">',
        '<option value="melee"',
        actionType === "melee" ? " selected" : "",
        '>Melee Attack</option>',
        '<option value="ranged"',
        actionType === "ranged" ? " selected" : "",
        '>Ranged Attack</option>',
        '<option value="other"',
        actionType === "other" ? " selected" : "",
        '>Other Action</option></select>',
        '<select class="attacktype">',
        '<option value="Weapon"',
        attackType === "Weapon" ? " selected" : "",
        '>Weapon Attack</option>',
        '<option value="Spell"',
        attackType === "Spell" ? " selected" : "",
        '>Spell Attack</option></select>',
        '<select class="rangedtype">',
        '<option value="fireforget"',
        rangedType === "fireforget" ? " selected" : "",
        '>Fire and Forget</option>',
        '<option value="area"',
        rangedType === "area" ? " selected" : "",
        '>Area</option>',
        '</select><div class="attackdamage">',
        '<input type="number" value="',
        damageDiceCount,
        '" min="1" class="dicecounter">&nbsp;<select class="dicetype">',
        '<option value="4"',
        damageDiceType === "4" ? " selected" : "",
        '>d4</option>',
        '<option value="6"',
        damageDiceType === "6" ? " selected" : "",
        '>d6</option>',
        '<option value="8"',
        damageDiceType === "8" ? " selected" : "",
        '>d8</option>',
        '<option value="10"',
        damageDiceType === "10" ? " selected" : "",
        '>d10</option>',
        '<option value="12"',
        damageDiceType === "12" ? " selected" : "",
        '>d12</option>',
        '<option value="20"',
        damageDiceType === "20" ? " selected" : "",
        '>d20</option>',
        '<option value="100"',
        damageDiceType === "100" ? " selected" : "",
        '>d100</option>',
        '</select><span>&nbsp;+&nbsp;</span><input type="number" value="',
        damageBonus,
        '" class="dicemodifier">&nbsp;<select class="damagetype"><option',
        damageType === "acid" ? " selected" : "",
        '>acid</option><option',
        damageType === "bludgeoning" ? " selected" : "",
        '>bludgeoning</option><option',
        damageType === "cold" ? " selected" : "",
        '>cold</option><option',
        damageType === "fire" ? " selected" : "",
        '>fire</option><option',
        damageType === "force" ? " selected" : "",
        '>force</option><option',
        damageType === "lightning" ? " selected" : "",
        '>lightning</option><option',
        damageType === "necrotic" ? " selected" : "",
        '>necrotic</option><option',
        damageType === "piercing" ? " selected" : "",
        '>piercing</option><option',
        damageType === "poison" ? " selected" : "",
        '>poison</option><option',
        damageType === "psychic" ? " selected" : "",
        '>psychic</option><option',
        damageType === "radiant" ? " selected" : "",
        '>radiant</option><option',
        damageType === "slashing" ? " selected" : "",
        '>slashing</option><option',
        damageType === "thunder" ? " selected" : "",
        '>thunder</option>',
        '</select><span>&nbsp;Dmg.</span></div><div class="meleedetails">',
        '<span>Range:&nbsp;</span><input type="number" value="',
        attackRange,
        '" step="5" min="0" class="rangemelee"><span>&nbsp;ft.</span></div>',
        '<div class="fireforgetdetails"><span>Range:&nbsp;</span>',
        '<input type="number" value="',
        rangeMin,
        '" step="5" min="0" class="rangeshort"><span>&nbsp;/&nbsp;</span>',
        '<input type="number" value="',
        rangeMax,
        '" step="5" min="0"  class="rangelong"><span>&nbsp;ft.</span></div>',
        '<div class="areadetails"><span>Shape:&nbsp;</span>',
        '<input type="number" value="',
        areaSize,
        '" step="5" min="0" class="areasize"><span>&nbsp;ft.&nbsp;</span>',
        '<select class="shapetype"><option',
        attackShape === "Cone" ? " selected" : "",
        '>Cone</option><option',
        attackShape === "Cube" ? " selected" : "",
        '>Cube</option><option',
        attackShape === "Cylinder" ? " selected" : "",
        '>Cylinder</option><option',
        attackShape === "Line" ? " selected" : "",
        '>Line</option><option',
        attackShape === "Sphere" ? " selected" : "",
        '>Sphere</option>',
        '</select></div><div class="nonareadetails"><span>+&nbsp;</span>',
        '<input type="number" value="',
        toHit,
        '" class="tohit"><span>&nbsp;to Hit.</span></div><textarea>',
        description,
        '</textarea></li>'
    );
    $("#actions").append(newActionHTML);
    updatePreview();
}
function addBlankReaction() {
    addReaction("New Reaction", "Description goes here.");
}
function addReaction(name, description) {
    console.log("Adding Reaction Pod.");
    var newReactionHTML = "".concat(
        '<li class="ui-state-default"><input type="text" value="',
        name,
        '" class="listitemtitle"><button class="deletebutton">X</button><textarea>',
        description,
        '</textarea></li>'
    );
    $("#reactions").append(newReactionHTML);
    updatePreview();
}
function addBlankSpell() {
    addSpell("", 0);
}
function addSpell(name, level) {
    console.log("Adding Spell Pod.");
    var newSpellHTML = '<li class="ui-state-default">'.concat(
            '<input type="text" value="',
            name,
            '" class="spellname" title="Spell Name" list="level0spells">\n',
            '<input type="number" value="',
            level,
            '" min="0" max="9" class="spelllevel" title="Spell Level">',
            '<button class="deletebutton">X</button></li>'
            );
    $("#spells").append(newSpellHTML);
    updatePreview();
}
function addBlankLegendary() {
    addLegendary("New Legendary Action", "Description goes here.");
}
function addLegendary(name, description) {
    console.log("Adding Legendary Action Pod");
    var newLegendaryHTML = '<li class="ui-state-default">'.concat(
        '<input type="text" value="',
        name,
        '" class="listitemtitle"><button class="deletebutton">X</button><textarea>',
        description,
        '</textarea></li>');
    $("#legendaryactions .sortablelist").append(newLegendaryHTML);
    updatePreview();
}