function convertMonsterToJSON() {
    var monsterObject = {
        name: $('input[name="monstername"]').val(),
        size: $('.monstersize option:selected').text(),
        type: $('input[name="monstertype"]').val(),
        tag: $('input[name="monstertag"]').val(),
        alignment: $('.monsteralignment option:selected').val(),
        armorClass: $('input[name="armorclass"]').val(),
        hitPointDiceCount: $('input[name="hitdicecount"]').val(),
        hitPointDiceType: $('.hitdicetype option:selected').val(),
        hitPointModifier: $('input[name="hitdicemodifier"]').val(),
        speed: $('input[name="speed"]').val(),
        challengeRating: $('.challengerating option:selected').html(),
        pronoun: $('select[name="pronoun"] option:selected').val(),
        strScore: $('input[name="strscore"]').val(),
        dexScore: $('input[name="dexscore"]').val(),
        conScore: $('input[name="conscore"]').val(),
        intScore: $('input[name="intscore"]').val(),
        wisScore: $('input[name="wisscore"]').val(),
        chaScore: $('input[name="chascore"]').val(),
        strBonus: $('input[name="strsave"]').val(),
        dexBonus: $('input[name="dexsave"]').val(),
        conBonus: $('input[name="consave"]').val(),
        intBonus: $('input[name="intsave"]').val(),
        wisBonus: $('input[name="wissave"]').val(),
        chaBonus: $('input[name="chasave"]').val(),
        passivePerception: $('input[name="passiveperception"]').val(),
        blindsight: $('input[name="blindsight"]').val(),
        darkvision: $('input[name="darkvision"]').val(),
        truesight: $('input[name="truesight"]').val(),
        tremorsense: $('input[name="tremorsense"]').val()
    };
    //Skills
    monsterObject.skills = {};
    $(".skillproficiencies").find("tr").each(function() {
        var newSkill = $(this).find("option:selected").val();
        var newSkillMod = $(this).find("input").val();
        monsterObject.skills[newSkill] = newSkillMod;
    });
    
    //Conditions
    monsterObject.conditions = {
        "blinded": $('select[name="blinded"] option:selected').html(),
        "charmed": $('select[name="charmed"] option:selected').html(),
        "deafened": $('select[name="deafened"] option:selected').html(),
        "encumbered": $('select[name="encumbered"] option:selected').html(),
        "exhausted": $('select[name="exhausted"] option:selected').html(),
        "frightened": $('select[name="frightened"] option:selected').html(),
        "grappled": $('select[name="grappled"] option:selected').html(),
        "incorporeal": $('select[name="incorporeal"] option:selected').html(),
        "intoxicated": $('select[name="intoxicated"] option:selected').html(),
        "invisible": $('select[name="invisible"] option:selected').html(),
        "paralyzed": $('select[name="paralyzed"] option:selected').html(),
        "petrified": $('select[name="petrified"] option:selected').html(),
        "poisoned": $('select[name="poisoned"] option:selected').html(),
        "prone": $('select[name="prone"] option:selected').html(),
        "restrained": $('select[name="restrained"] option:selected').html(),
        "stunned": $('select[name="stunned"] option:selected').html(),
        "unconscious": $('select[name="unconscious"] option:selected').html()
    };
    
    //Damage
    monsterObject.damage = {
        "acid": $('select[name="acid"] option:selected').html(),
        "bludgeoning": $('select[name="bludgeoning"] option:selected').html(),
        "cold": $('select[name="cold"] option:selected').html(),
        "fire": $('select[name="fire"] option:selected').html(),
        "force": $('select[name="force"] option:selected').html(),
        "lightning": $('select[name="lightning"] option:selected').html(),
        "necrotic": $('select[name="necrotic"] option:selected').html(),
        "piercing": $('select[name="piercing"] option:selected').html(),
        "poison": $('select[name="poison"] option:selected').html(),
        "psychic": $('select[name="psychic"] option:selected').html(),
        "radiant": $('select[name="radiant"] option:selected').html(),
        "slashing": $('select[name="slashing"] option:selected').html(),
        "thunder": $('select[name="thunder"] option:selected').html(),
        "nonmagic": $('select[name="nonmagic"] option:selected').html(),
    };
    
    //Languages
    monsterObject.languages = {};
    var langIter = 0;
    $("#languages input").each(function() {
        monsterObject.languages[langIter] = $(this).val();
        langIter++;
    });
    
    //Abilities
    monsterObject.abilities = {};
    var abilityIter = 0;
    $("#abilities .ui-state-default").each(function() {
        var abilityName = $(this).children("input").val();
        var abilityDescription = $(this).children("textarea").val();
        monsterObject.abilities[abilityIter] = {abilityName, abilityDescription};
        abilityIter++;
    });
    
    //Spellcaster
    monsterObject.spellcaster = {};
    monsterObject.spellcaster["enabled"] = $("#spellcaster .spellcastingenabled").is(':checked');
    monsterObject.spellcaster["spellcastingLevel"] = $('input[name="spellcastinglevel"]').val();
    monsterObject.spellcaster["spellcastingToHit"] = $('input[name="spellcastingtohit"]').val();
    monsterObject.spellcaster["spellcastingAbility"] = $('select[name="spellcastingability"] option:selected').html();
    monsterObject.spellcaster["spellcastingDC"] = $('input[name="spellcastingdc"]').val();
    monsterObject.spellcaster["spellcastingClass"] = $('input[name="spellcastingclass"]').val();
    monsterObject.spellcaster["knownSpells"] = {};
    $("#spells").find("li").each(function() {
        var spellName = $(this).find(".spellname").val();
        var spellLevel = parseInt($(this).find(".spelllevel").val());
        monsterObject.spellcaster["knownSpells"][spellName] = spellLevel;
    });
    monsterObject.spellcaster["spellcastingSlots"] = {};
    for (var i = 0; i <= 9; i++) {
        var spellSlots = parseInt($(".spelllevels").find('input[name=level'.concat(i,']')).val());
        monsterObject.spellcaster["spellcastingSlots"][i] = spellSlots;
    }
    
    //Actions
    monsterObject.actions = {};
    var actionIter = 0;
    $("#actions .ui-state-default").each(function() {
        var actionType = $(this).find(".actiontype option:selected").val();
        var actionName = $(this).find('input[name="actionname"]').val();
        var actionDescription = $(this).find('textarea').val();
        var attackDamageDiceCount = $(this).find(".dicecounter").val();
        var attackDamageDiceType = $(this).find(".dicetype option:selected").val();
        var attackDamageBonus = $(this).find(".dicemodifier").val();
        var attackDamageType = $(this).find(".damagetype").val();
        var attackType = $(this).find(".attacktype option:selected").val();
        var attackRange = $(this).find(".rangemelee").val();
        var toHit = $(this).find(".tohit").val();
        var rangedType = $(this).find(".rangedtype option:selected").val();
        var rangeMin = $(this).find(".rangeshort").val();
        var rangeMax = $(this).find(".rangelong").val();
        var attackShape = $(this).find(".shapetype option:selected").html();
        var attackSize = $(this).find(".areasize").val();
        monsterObject.actions[actionIter] = {
            actionType,actionName,actionDescription,attackDamageDiceCount,
            attackDamageDiceType,attackDamageBonus,attackDamageType,attackType,
            attackRange,toHit,rangedType,rangeMin,rangeMax,attackShape,attackSize
        };
        actionIter++;
    });
    
    //Legendary Actions
    monsterObject.legendaryUses = $('input[name="legendarycount"]').val();
    monsterObject.legendaryActions = {};
    var legendaryIter = 0;
    $("#legendaryactions .ui-state-default").each(function() {
        var legendaryName = $(this).children("input").val();
        var legendaryDescription = $(this).children("textarea").val();
        monsterObject.legendaryActions[legendaryIter] = {legendaryName, legendaryDescription};
        legendaryIter++;
    });
    
    //Reactions
    monsterObject.reactions = {};
    var reactionIter = 0;
    $("#reactions .ui-state-default").each(function() {
        var reactionName = $(this).children("input").val();
        var reactionDescription = $(this).children("textarea").val();
        monsterObject.reactions[reactionIter] = {reactionName, reactionDescription};
        reactionIter++;
    });
    
    
    //Download JSON File
    console.log(JSON.stringify(monsterObject));
    saveTemplateAsFile(monsterObject.name.concat(".json"), monsterObject);
}
const saveTemplateAsFile = (filename, dataObjToWrite) => {
    //https://stackoverflow.com/questions/19721439/download-json-object-as-a-file-from-browser
    const blob = new Blob([JSON.stringify(dataObjToWrite)], { type: "text/json" });
    const link = document.createElement("a");

    link.download = filename;
    link.href = window.URL.createObjectURL(blob);
    link.dataset.downloadurl = ["text/json", link.download, link.href].join(":");

    const evt = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
    });

    link.dispatchEvent(evt);
    link.remove();
};
function toggleFileUploadMenu() {
    if ($("#fileupload").is(":visible")) {
        $("#fileupload").hide();
    } else {
        $("#fileupload").show();
    }
}
function promptToImportMonsterFromFile() {
    var prompt = "Uploading a new monster will delete all information contained within the monster input form. Press OK to continue.";
    if (confirm(prompt)) {
        var importedFile = $("#fileupload input").prop('files')[0];
        var reader = new FileReader();
        reader.onload = function() {
            importMonsterFromJSON(reader.result);
        };
        reader.readAsText(importedFile);
        toggleFileUploadMenu();
    } else {
        console.log("Monster upload was cancelled.");
    }
}
function importMonsterFromJSON(jsonString) {
    var monsterObject = JSON.parse(jsonString);
    console.log("Loading Monster from File: ".concat(monsterObject.name));
    
    //Basic Info
    $('input[name="monstername"]').val(monsterObject.name);
    $(".monstersize option:contains('".concat(monsterObject.size,"')")).prop('selected', true);
    $('input[name="monstertype"]').val(monsterObject.type);
    $('input[name="monstertag"]').val(monsterObject.tag);
    $(".monsteralignment option:contains('".concat(monsterObject.alignment,"')")).prop('selected', true);
    $('input[name="armorclass"]').val(monsterObject.armorClass);
    $('input[name="hitdicecount"]').val(monsterObject.hitPointDiceCount);
    $(".hitdicetype option[value='".concat(monsterObject.hitPointDiceType,"']")).prop('selected', true);
    $('input[name="hitdicemodifier"]').val(monsterObject.hitPointModifier);
    $('input[name="speed"]').val(monsterObject.speed);
    $("select[name='pronoun'] option[value='".concat(monsterObject.pronoun,"']")).prop('selected', true);
    $('input[name="strscore"]').val(monsterObject.strScore);
    $('input[name="dexscore"]').val(monsterObject.dexScore);
    $('input[name="conscore"]').val(monsterObject.conScore);
    $('input[name="intscore"]').val(monsterObject.intScore);
    $('input[name="wisscore"]').val(monsterObject.wisScore);
    $('input[name="chascore"]').val(monsterObject.chaScore);
    $('input[name="strsave"]').val(monsterObject.strBonus);
    $('input[name="dexsave"]').val(monsterObject.dexBonus);
    $('input[name="consave"]').val(monsterObject.conBonus);
    $('input[name="intsave"]').val(monsterObject.intBonus);
    $('input[name="wissave"]').val(monsterObject.wisBonus);
    $('input[name="chasave"]').val(monsterObject.chaBonus);
    $('input[name="passiveperception"]').val(monsterObject.passivePerception);
    $('input[name="blindsight"]').val(monsterObject.blindsight);
    $('input[name="darkvision"]').val(monsterObject.darkvision);
    $('input[name="truesight"]').val(monsterObject.truesight);
    $('input[name="tremorsense"]').val(monsterObject.tremorsense);
    
    //Challenge Rating
    $(".challengerating").find("option").each(function() {
        if ($(this).html() === monsterObject.challengeRating) {
            $(this).prop('selected', true);
        }
    });
    
    //Skills
    $(".skillproficiencies").find("tr").remove();
    Object.keys(monsterObject.skills).forEach(function(index){
        addSkill(index,monsterObject.skills[index]);
    });
    
    //Conditions
    $("#conditionsanddamage option:contains('Unchanged')").prop('selected', true);
    Object.keys(monsterObject.conditions).forEach(function(index){
        $("select[name='".concat(index,"'] option:contains('",monsterObject.conditions[index],"')")).prop('selected', true);
    });
    
    //Damage
    Object.keys(monsterObject.damage).forEach(function(index){
        $("select[name='".concat(index,"'] option:contains('",monsterObject.damage[index],"')")).prop('selected', true);
    });
    
    //Language
    $("#languages").find("li").remove();
    Object.keys(monsterObject.languages).forEach(function(index){
        addLanguage(monsterObject.languages[index]);
    });
    
    //Abilities
    $("#abilities").find("li").remove();
    Object.keys(monsterObject.abilities).forEach(function(index){
        var abilityName = monsterObject.abilities[index]["abilityName"];
        var abilityDescription = monsterObject.abilities[index]["abilityDescription"];
        addAbility(abilityName, abilityDescription);
    });
    
    //Actions
    $("#actions").find("li").remove();
    Object.keys(monsterObject.actions).forEach(function(index){
        var actionType = monsterObject.actions[index]["actionType"];
        var actionName = monsterObject.actions[index]["actionName"];
        var actionDescription = monsterObject.actions[index]["actionDescription"];
        var attackDamageDiceCount = monsterObject.actions[index]["attackDamageDiceCount"];
        var attackDamageDiceType = monsterObject.actions[index]["attackDamageDiceType"];
        var attackDamageBonus = monsterObject.actions[index]["attackDamageBonus"];
        var attackDamageType = monsterObject.actions[index]["attackDamageType"];
        var attackType = monsterObject.actions[index]["attackType"];
        var attackRange = monsterObject.actions[index]["attackRange"];
        var toHit = monsterObject.actions[index]["toHit"];
        var rangedType = monsterObject.actions[index]["rangedType"];
        var rangeMin = monsterObject.actions[index]["rangeMin"];
        var rangeMax = monsterObject.actions[index]["rangeMax"];
        var attackShape = monsterObject.actions[index]["attackShape"];
        var attackSize = monsterObject.actions[index]["attackSize"];
        
        addAction(actionName, actionDescription, actionType, attackDamageDiceCount, attackDamageDiceType, 
        attackDamageBonus, attackDamageType, attackType, attackRange, toHit, rangedType, 
        rangeMin, rangeMax, attackShape, attackSize);
    });
    
    //Reactions
    $("#reactions").find("li").remove();
    Object.keys(monsterObject.reactions).forEach(function(index){
        var reactionName = monsterObject.reactions[index]["reactionName"];
        var reactionDescription = monsterObject.reactions[index]["reactionDescription"];
        addReaction(reactionName, reactionDescription);
    });
    
    //Legendary Actions
    $('input[name="legendarycount"]').val(monsterObject.legendaryUses);
    $("#legendaryactions").find("li").remove();
    Object.keys(monsterObject.legendaryActions).forEach(function(index){
        var legendaryName = monsterObject.legendaryActions[index]["legendaryName"];
        var legendaryDescription = monsterObject.legendaryActions[index]["legendaryDescription"];
        addLegendary(legendaryName, legendaryDescription);
    });
    
    //Spellcaster
    $(".spellcastingenabled").prop("checked", monsterObject.spellcaster.enabled);
    $('input[name="spellcastingclass"]').val(monsterObject.spellcaster.spellcastingClass);
    $('select[name="spellcastingability"] option:contains("'.concat(monsterObject.spellcaster.spellcastingAbility,'")')).prop('selected', true);
    $('input[name="spellcastinglevel"]').val(monsterObject.spellcaster.spellcastingLevel);
    $('input[name="spellcastingdc"]').val(monsterObject.spellcaster.spellcastingDC);
    $('input[name="spellcastingtohit"]').val(monsterObject.spellcaster.spellcastingToHit);
    for (var i = 1; i <= 9; i++) {
        $('.spelllevels input[name="level'.concat(i,'"]')).val(monsterObject.spellcaster.spellcastingSlots[i]);
    }
    $("#spells").find("li").remove();
    Object.keys(monsterObject.spellcaster.knownSpells).forEach(function(index){
        var spellName = index;
        var spellLevel = monsterObject.spellcaster.knownSpells[index];
        addSpell(spellName, spellLevel);
    });
    updatePreview();
}
function promptToResetForm() {
    var prompt = "Doing this will delete all information contained within the monster input form. Press OK to continue.";
    if (confirm(prompt)) {
        resetForm();
    } else {
        console.log("Form reset was cancelled.");
    }
}
function resetForm() {
    console.log("Resetting monster input form.");
    $('input[name="monstername"]').val("");
    $(".monstersize option:contains('Medium')").prop('selected', true);
    $('input[name="monstertype"]').val("");
    $('input[name="monstertag"]').val("");
    $(".monsteralignment option:contains('Any Alignment')").prop('selected', true);
    $('input[name="armorclass"]').val("");
    $('input[name="hitdicecount"]').val("0");
    $('.hitdicetype option[value="4"]').prop('selected', true);
    $('input[name="hitdicemodifier"]').val("0");
    $('input[name="speed"]').val("30");
    $('input[name="passiveperception"]').val("10");
    $('select[name="pronoun"] option[value="IT"]').prop('selected', true);
    $(".challengerating option:first").prop('selected', true);
    $('input[name="strscore"]').val("10");
    $('input[name="dexscore"]').val("10");
    $('input[name="conscore"]').val("10");
    $('input[name="intscore"]').val("10");
    $('input[name="wisscore"]').val("10");
    $('input[name="chascore"]').val("10");
    $('input[name="strsave"]').val("0");
    $('input[name="dexsave"]').val("0");
    $('input[name="consave"]').val("0");
    $('input[name="intsave"]').val("0");
    $('input[name="wissave"]').val("0");
    $('input[name="chasave"]').val("0");
    $('input[name="blindsight"]').val("0");
    $('input[name="darkvision"]').val("0");
    $('input[name="truesight"]').val("0");
    $('input[name="tremorsense"]').val("0");
    $(".skillproficiencies").find("tr").remove();
    $("#conditionsanddamage option:contains('Unchanged')").prop('selected', true);
    $("#languages").find("li").remove();
    $("#abilities").find("li").remove();
    $("#actions").find("li").remove();
    $("#reactions").find("li").remove();
    $('input[name="legendarycount"]').val("0");
    $("#legendaryactions").find("li").remove();
    $(".spellcastingenabled").prop( "checked", false );
    $('input[name="spellcastingclass"]').val("");
    $('select[name="spellcastingability"] option:first').prop('selected', true);
    $('input[name="spellcastinglevel"]').val("1");
    $('input[name="spellcastingdc"]').val("10");
    $('input[name="spellcastingtohit"]').val("0");
    $(".spelllevels").find('input').val("0");
    $("#spells").find("li").remove();
    updatePreview();
}